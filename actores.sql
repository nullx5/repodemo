-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para actores
CREATE DATABASE IF NOT EXISTS `actores` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `actores`;

-- Volcando estructura para tabla actores.movies_al_pacino
CREATE TABLE IF NOT EXISTS `movies_al_pacino` (
  `idmovies_al_pacino` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmovies_al_pacino`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.movies_al_pacino: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `movies_al_pacino` DISABLE KEYS */;
INSERT INTO `movies_al_pacino` (`idmovies_al_pacino`, `nombre`, `miniatura`) VALUES
	(1, 'El padrino', '25'),
	(2, 'El precio del poder', '26'),
	(3, 'El irlandes', '27'),
	(4, ' El padrino 2', '28'),
	(5, 'Perfume de mujer', '29'),
	(6, 'Fuego contra fuego', '30'),
	(7, 'El abogado del diablo', '31'),
	(8, 'Atrapado por su pasado', '32');
/*!40000 ALTER TABLE `movies_al_pacino` ENABLE KEYS */;

-- Volcando estructura para tabla actores.movies_angelina_jolie
CREATE TABLE IF NOT EXISTS `movies_angelina_jolie` (
  `idmovies_angelina_jolie` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmovies_angelina_jolie`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.movies_angelina_jolie: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `movies_angelina_jolie` DISABLE KEYS */;
INSERT INTO `movies_angelina_jolie` (`idmovies_angelina_jolie`, `nombre`, `miniatura`) VALUES
	(1, 'Malefica', '1'),
	(2, 'Hackers', '2'),
	(3, 'El coleccionista', '3'),
	(4, 'Pecado original', '4'),
	(5, 'Sr y sra Smith', '5'),
	(6, 'El turista', '6'),
	(7, 'Inquebrantable', '7'),
	(8, 'Se busca', '8');
/*!40000 ALTER TABLE `movies_angelina_jolie` ENABLE KEYS */;

-- Volcando estructura para tabla actores.movies_mel_gibson
CREATE TABLE IF NOT EXISTS `movies_mel_gibson` (
  `idmovies_mel_gibson` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmovies_mel_gibson`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.movies_mel_gibson: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `movies_mel_gibson` DISABLE KEYS */;
INSERT INTO `movies_mel_gibson` (`idmovies_mel_gibson`, `nombre`, `miniatura`) VALUES
	(1, 'La pasion de cristo', '9'),
	(2, 'Corazon valiente', '10'),
	(3, 'El patriota', '11'),
	(4, 'Hasta el ultimo hombre', '12'),
	(5, 'Mad max', '13'),
	(6, 'En que piensan las mujeres', '14'),
	(7, 'Entre la razon y la locura', '15'),
	(8, 'Apocalypto', '16');
/*!40000 ALTER TABLE `movies_mel_gibson` ENABLE KEYS */;

-- Volcando estructura para tabla actores.movies_meryl_streep
CREATE TABLE IF NOT EXISTS `movies_meryl_streep` (
  `idmovies_meryl_streep` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmovies_meryl_streep`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.movies_meryl_streep: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `movies_meryl_streep` DISABLE KEYS */;
INSERT INTO `movies_meryl_streep` (`idmovies_meryl_streep`, `nombre`, `miniatura`) VALUES
	(1, 'El diablo viste a la moda', '17'),
	(2, 'Los puentes de madison', '18'),
	(3, 'Memorias de africa', '19'),
	(4, 'La desicion de sophie', '20'),
	(5, 'La muerte le sienta bien', '21'),
	(6, 'La dama de hierro', '22'),
	(7, 'Rio salvaje', '23'),
	(8, 'L a casa de los espiritus', '24');
/*!40000 ALTER TABLE `movies_meryl_streep` ENABLE KEYS */;

-- Volcando estructura para tabla actores.movies_robert_de_niro
CREATE TABLE IF NOT EXISTS `movies_robert_de_niro` (
  `idmovies_robert_de_niro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `miniatura` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmovies_robert_de_niro`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.movies_robert_de_niro: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `movies_robert_de_niro` DISABLE KEYS */;
INSERT INTO `movies_robert_de_niro` (`idmovies_robert_de_niro`, `nombre`, `miniatura`) VALUES
	(1, 'Buenos muchachos', '33'),
	(2, 'Taxi driver', '34'),
	(3, 'El casino', '35'),
	(4, 'Analizame', '36'),
	(5, 'Hombres de honor', '37'),
	(6, 'Los hijos de la calle', '38'),
	(7, 'Amor a primera vista', '39'),
	(8, 'Herencia de sangre', '40');
/*!40000 ALTER TABLE `movies_robert_de_niro` ENABLE KEYS */;

-- Volcando estructura para tabla actores.registro_de_eventos
CREATE TABLE IF NOT EXISTS `registro_de_eventos` (
  `idregistro` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(12) NOT NULL,
  `tabla` varchar(12) NOT NULL,
  `accion` varchar(25) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`idregistro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.registro_de_eventos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `registro_de_eventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro_de_eventos` ENABLE KEYS */;

-- Volcando estructura para tabla actores.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(12) NOT NULL,
  `password` varchar(15) NOT NULL,
  `nivel` int(2) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla actores.usuarios: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idusuario`, `nombre`, `password`, `nivel`) VALUES
	(1, 'luz', '12345', 0),
	(2, 'carlos perez', '76543', 0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
