<!DOCTYPE html>
<?php
//========== abre conexion con base de datos "actores"==========
$conexion = mysqli_connect("localhost", "root", "", "actores") or
    die("problemas con la conexion");

//========== realiza consultas sql select a las diferentes tablas de la DB actores ==========

$reg_angelina_jolie = mysqli_query($conexion, "select idmovies_angelina_jolie, nombre, miniatura from movies_angelina_jolie") or
    die("problemas en el select:" . mysqli_error($conexion));

$reg_mel_gibson = mysqli_query($conexion, "select idmovies_mel_gibson, nombre, miniatura from movies_mel_gibson") or
    die("problemas en el select:" . mysqli_error($conexion));

$reg_meryl_streep = mysqli_query($conexion, "select idmovies_meryl_streep, nombre, miniatura from movies_meryl_streep") or
    die("problemas en el select:" . mysqli_error($conexion));

$reg_al_pacino = mysqli_query($conexion, "select idmovies_al_pacino, nombre, miniatura from movies_al_pacino") or
    die("problemas en el select:" . mysqli_error($conexion));

$reg_robert_de_niro = mysqli_query($conexion, "select idmovies_robert_de_niro, nombre, miniatura from movies_robert_de_niro") or
    die("problemas en el select:" . mysqli_error($conexion));
?>

<html lang="en">

<head>
    <title>Maquetacion web</title>
    <!-- ================================= iconos del logo ===============================================-->
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
    <!-- ============================== Bootstrap CSS mejorado =========================================== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- ============================ Fuente de google ==================================================-->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap" rel="stylesheet">
    <style>
        body {
            background-color: #F3F3F3;
        }

        .container-fluid {
            background-color: #E2E2E2;
        }

        #header {
            width: 100%;
            height: 70px;
            left: 326px;
            top: 50px;
            margin: 50px 0px 40px 0px;
            padding-top: 20px;


        }

        #logotipo {
            float: left;
            padding-bottom: 10px;


        }

        input {
            margin: 5px;
        }

        .enlinea {
            font-family: consolas;
            display: inline;

        }

        #buscador {
            float: right;
            text-align: right;
        }

        .caja {
            background-color: white;
            border-radius: 5px;
        }

        #resultados {
            margin: 30px 0 15px 20px;
        }

        #peli_principal {
            border-radius: 5px;
        }

        .filas {
            width: 100%;
            height: 210px;
            left: 456px;
            top: 274px;
        }

        .col-md-3 {

            width: 124px;
            height: 189px;
            margin: 100px;
            padding: 0px;
            background: url(image.png);
            border: 2px solid #A3A0A0;
            box-shadow: inset 0px 4px 50px rgba(0, 0, 0, 0.25);

        }

        .col-md-4 {
            width: 243px;
            height: 250px;
            margin: 90px;
            padding: 0px;
            background: url(image.png);
            border: 3px solid #000000;
            box-shadow: 0px 4px 100px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
        }
        .banner{
            margin-right: 30px;
            margin-left: 30px;
        }
        .form{
            margin-left: 30px;
        }
        #footer {
            background-color: #242424;
            color: white;
            height: 250px;
            padding: 30px 0 30px 0;

        }
    </style>
</head>

<body>
    <div class="container-fluid">

        <!-- ============================= HEADER ========================================= -->
        <div class="row">
            <div id="header">
                <div class="col-md-6" id="logotipo">
                    <span class="iconify enlinea" data-inline="false" data-icon="mdi:movie-open-outline" style="font-size: 40px;"></span>
                    <p class="enlinea" style="font-size: 40px; color:#d8322f;">MovieActorSearch</p>
                    <span class="iconify enlinea" data-inline="false" data-icon="fxemoji:moviecamera" style="font-size: 40px;"></span>
                </div>
                <div class="col-md-6" id="buscador">
                    <form action="8.2cms.php" method="post">
                        <input type="text" class="form-control enlinea" id="busqueda" placeholder="Buscar Actor" style="width: 30%; font-family: 'Noto Sans', sans-serif;" name="txtbusqueda">
                        <br>
                        <input type="submit" class="btn btn-danger enlinea" id="buscar" value="Buscar" name="buscar">
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================= CONTENIDO ====================================== -->
        <div class="container-fluid caja">
            <!-- =============== Texto Resultado y Peliculas Encontradas =====================-->
            <div class="row">
                <div id="resultados">
                    <div class="col-md-12">
                        <p style="font-size: 16px; text-align:right;"> <strong style="color:#04B10B;">Resultados</strong>
                        </p>
                    </div>
                </div>
            </div>

            <!--================= PHP ==================-->
            <?php
            if (isset($_POST["buscar"])) {
                $busqueda = $_POST["txtbusqueda"];

                // ================== Condicional Switch Case ===============
                switch ($busqueda) {
                    case "angelina jolie":
                        echo "<div class='row'>";
                        echo "<div id='peli_principal'>";

                        while ($reg1 = mysqli_fetch_array($reg_angelina_jolie)) {
                            echo "<div class='col-md-3' style='background-color:yellow;'>";
                            echo "<img src='images/" . $reg1['miniatura'] . ".jpg' alt='" . $reg1['nombre'] .  "' width='160' height='255'>";
                            echo "<p style='font-size: 16px; color: black;' align='center'>" . $reg1['nombre'] . "</p>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        break;

                    case "mel gibson":
                        echo "<div class='row'>";
                        echo "<div id='peli_principal'>";

                        while ($reg2 = mysqli_fetch_array($reg_mel_gibson)) {
                            echo "<div class='col-md-3' style='background-color:yellow;'>";
                            echo "<img src='images/" . $reg2['miniatura'] . ".jpg' alt='" . $reg2['nombre'] .  "' width='160' height='255'>";
                            echo "<p style='font-size: 16px; color: black;' align='center'>" . $reg2['nombre'] . "</p>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        break;
                    case "meryl streep":
                        echo "<div class='row'>";
                        echo "<div id='peli_principal'>";

                        while ($reg3 = mysqli_fetch_array($reg_meryl_streep)) {
                            echo "<div class='col-md-3' style='background-color:yellow;'>";
                            echo "<img src='images/" . $reg3['miniatura'] . ".jpg' alt='" . $reg3['nombre'] .  "' width='160' height='255'>";
                            echo "<p style='font-size: 16px; color: black;' align='center'>" . $reg3['nombre'] . "</p>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        break;
                    case "al pacino":
                        echo "<div class='row'>";
                        echo "<div id='peli_principal'>";

                        while ($reg4 = mysqli_fetch_array($reg_al_pacino)) {
                            echo "<div class='col-md-3' style='background-color:yellow;'>";
                            echo "<img src='images/" . $reg4['miniatura'] . ".jpg' alt='" . $reg4['nombre'] .  "' width='160' height='255'>";
                            echo "<p style='font-size: 16px; color: black;' align='center'>" . $reg4['nombre'] . "</p>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        break;
                    case "robert de niro":
                        echo "<div class='row'>";
                        echo "<div id='peli_principal'>";

                        while ($reg5 = mysqli_fetch_array($reg_robert_de_niro)) {
                            echo "<div class='col-md-3' style='background-color:yellow;'>";
                            echo "<img src='images/" . $reg5['miniatura'] . ".jpg' alt='" . $reg5['nombre'] .  "' width='160' height='255'>";
                            echo "<p style='font-size: 16px; color: black;' align='center'>" . $reg5['nombre'] . "</p>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        break;
                }
            }

            ?>

            <!-- ================ Texto Actores Recomendados ==================== -->
            <div class="row">
                <div id="recomendaciones">
                    <div class="col-md-12">
                        <p align="center"><span class="iconify" data-inline="false" data-icon="ri:movie-2-line" style="font-size: 40px;"></span></p>
                        <p style="font-size: 20px;" align="center"> <strong style="color:#04B10B;"> Actores Recomendados</strong>
                        </p>
                    </div>
                </div>
            </div>
            <!-- =============== Imagenes Actores Recomendados =================-->
            <div class="row">
                <div id="main_actores">
                    <div class="col-md-4" id="actor_one">
                        <img src="images/mel_gibson.jpg" alt="Mel Gibson" width="237px" height=244px;">
                        <p style="font-size: 16px; color: black;" align="center">mel gibson</p>
                    </div>
                    <div class="col-md-4" id="actor_two">
                        <img src="images/meryl_streep.jpg" alt="Meryl Streep" width="237px" height=244px;">
                        <p style="font-size: 16px; color: black;" align="center">meryl streep</p>
                    </div>
                    <div class="col-md-4" id="actor_tree">
                        <img src="images/al_pacino.jpg" alt="Al Pacino" width="237px" height=244px;">
                        <p style="font-size: 16px; color: black;" align="center">al pacino</p>
                    </div>
                </div>
            </div>
            <br>
            <hr><br>
            <div class="row">
                <div class="col-md-4 banner" style="margin-bottom: 30px;">
                    <img src="cartelera.png" alt="cartelera" width="238" height="244" style="border-radius: 3px;">
                </div>
                
                <div class="col-md-2" style="margin-top:80px; width:300px;">
                    <h3>MovieActorSearch</h3>
                    <p style="text-align: justify;">
                        En este sitio rendimos un pequeño homenaje a los grandes actores de cine:
                         <strong>mel gibson</strong>, <strong>meryl streep</strong>, <strong>al pacino</strong>, <strong>robert de niro</strong> y <strong>angelina jolie</strong>, 
                        que por años nos han entretenido con sus maravillosos films.
                         Aquí encontraras una corta seleccion de sus principales y mas taquilleras 
                         peliculas, esperamos las disfruten y sean de su agrado.
                         Para ver su filmografia ingrese los nombres como aparecen aqui.
                    </p>
                </div>
                <div class="col-md-6 form">
                    <h2>Ingresar</h2><br><br> <br>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="usuario">Usuario:</label>
                            <input type="text" class="form-control" id="usuario" placeholder="Ingrese Usuario" name="txtusuario">
                        </div>
                       
                        <div class="form-group">
                            <label for="clave">Clave:</label>
                            <input type="password" class="form-control" id="clave" placeholder="Ingrese Clave" name="txtclave">
                        </div>

                        <button type="submit" class="btn btn-danger" name="enviar">Enviar</button>

                    </form>

                </div>
            </div>
            <hr>
            <?php
            if (isset($_POST["enviar"])) {
                $usuario = $_POST["txtusuario"];
                $clave = $_POST["txtclave"];
                // ================== Realiza consula SQL ingresa datos en la tabla usuarios =====
                mysqli_query($conexion, "insert into usuarios(nombre, password) values ('$usuario', '$clave')") or
                    die("Problemas en el select" . mysqli_error($conexion));
            }
            $registro = mysqli_query($conexion, "select idusuario, nombre, password from usuarios") or
                die("problemas en el select" . mysqli_error($conexion));
            // Tabla 
            echo "<div class='container'>";
            echo "<table class='table table-striped table-dark'>";
            echo "<tr>";
            echo "<td style='color: #04B10B;'><strong>ID</trong></td>";
            echo "<td style='color: #04B10B;'><strong>USUARIO</trong></td>";
            echo "<td style='color: #04B10B;'><strong>PASSWORD</trong></td>";

            echo "</tr>";
            while ($reg = mysqli_fetch_array($registro)) {
                echo "<tr>";
                echo "<td>" . $reg['idusuario'] . "</td>";
                echo "<td>" . $reg['nombre'] . "</td>";
                echo "<td>" . $reg['password'] . "</td>";
                echo "</tr>";
            }


            echo "</table>";
            echo "</div>";

            ?>

        </div>
        <br>
        <br>
        <br>
        <!-- ============================ FOOTER =========================================== -->
        <div class="row">
            <div id="footer">
                <div class="col-md-12">
                    <p style="font-size: 16px; font-family: 'Noto Sans', sans-serif;" align="center">Todos los Derechos Reservados 2020
                    </p>
                    <p style="font-size: 16px; font-family: 'Noto Sans', sans-serif;" align="center">Luz Marina Avila Gonzalez</p>
                    <p style="font-size: 16px; font-family: 'Noto Sans', sans-serif;" align="center">Marcela Garcia</p>
                    <p style="font-size: 16px; font-family: 'Noto Sans', sans-serif;" align="center">Johan Bloom</p>
                    <h3 style="font-size: 18px;" align="center"><strong style="color:#FEB906;">COREDUCACION</strong></h3>
                    <p style="font-size: 18px;" align="center">Hecho con <span class="iconify" data-inline="false" data-icon="emojione:growing-heart" style="font-size: 40px;"></span></p>
                </div>
            </div>

        </div>
    </div>


</body>

</html>