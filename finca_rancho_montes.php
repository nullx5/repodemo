<!DOCTYPE html>
<?php
    //Enlace con la Base de Datos
    $conexion = mysqli_connect("localhost", "root", "", "finca") or
        die("Problemas con la conexión");
?>
<html lang="en">
    <head>
        <title>Finca Ranchos de los Montes</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Hermosa Finca Natural, para pasar las mejores vacaciones de tu vida">
        <meta name="author" content="Cristiam Villegas">
        <!-- Bootstrap-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <!-- Mis estilos CSS -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>

    <body>
        <!-- Menu de Navegacion -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Finca Ranchos de los Montes</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#cabañas">Cabañas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#registrarse">Registrarse</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Contenido de bienvenida y cabañas-->
        <div class="container">
            <!-- Bienvenida -->

            <header class="jumbotron my-4">
                <h1 class="display-3">Bienvenidos!</h1>
                <p class="lead">Finca recreacional ranchos de los montes ubicada en la vereda el caucho del municipio de san
                sebastian de mariquita, le ofrece confortables cabanas dotadas de: cocina, juegos deportivos, parqueadero, zonas
                verdes, piscina y lo mejor el mejor ambiente del campo y a tan solo 5 minutos del casco urbano del municipio.
                </p>
                <img src="http://localhost/elementos_cristiam/finca_aerea.jpg" alt="Foto Aerea Finca" width="100%" height="550px">
            </header>
            <!-- Cabañas -->

            <h2 id="cabañas" class="text-center">Cabañas</h2><br><br>
            <div class="row text-center">
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <img class="card-img-top" src="http://localhost/elementos_cristiam/finca_casaL.jpg" alt="cabaña 1" width="500" height="165">
                        <div class="card-body">
                            <h4 class="card-title">Nupcial</h4>
                            <p class="card-text">Nuestra espaciosa Cabaña Nupcial cuenta con una cómoda cama King de 2 mts de ancho por
                                2 mts de largo, sala y jacuzzi.
                            </p>
                        </div>

                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <img class="card-img-top" src="http://localhost/elementos_cristiam/finca_casaAZ.jpg" alt="cabaña 2" width="500" height="165">
                        <div class="card-body">
                            <h4 class="card-title">Empresarial</h4>
                            <p class="card-text">Nuestra Cabaña Empresarial cuenta con 3 cuartos, cómodas camas, sala de estar y
                            recreación
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <img class="card-img-top" src="http://localhost/elementos_cristiam/finca_casaA.jpg" alt="cabaña 3" width="500" height="165">
                        <div class="card-body">
                            <h4 class="card-title">Ejecutiva</h4>
                            <p class="card-text">Nuestra Cabaña Ejecutiva cuenta con 2 comodas habitaciones, servicio de minibar,
                            cómodas camas, y salas de esparcimiento y recreación
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <img class="card-img-top" src="http://localhost/elementos_cristiam/finca_habitacion2.jpg" alt="cabaña 4" width="500" height="165">
                        <div class="card-body">
                            <h4 class="card-title">Cabaña Familiar</h4>
                            <p class="card-text">Nuestra Cabaña Familiar cuenta con 1 habitaciones cama doble y 3 sencillas, baño, sala
                            con teatro en casa y una sala con juegos recreativos.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Formulario -->
        <div class='container'>
            <h2 id=registrarse align="center">Registrarse</h2>
            <form name="" action="" method="post">
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Nombre" name="txtNombre">
                    </div><br><br>

                    <div class="col">
                        <input type="text" class="form-control" placeholder="Apellido" name="txtApellido">
                    </div><br>

                    <div class="col">
                        <input type="text" class="form-control" placeholder="Cabaña" name="txtCabana">
                    </div><br>

                    <div class="col">
                        <input type="text" class="form-control" placeholder="Fecha de Registro" name="txtFecha">
                    </div>
                </div></br>

                <div class="text-center">
                    <button type="submit" name="enviar" class="btn btn-primary">Enviar</button><br><br>
                </div>
            </form>

            <!-- PHP capturando valor de los inputs  -->
            <?php
                if (isset($_POST["enviar"])) {
                    $nombre = $_POST["txtNombre"];
                    $apellido = $_POST["txtApellido"];
                    $cabana = $_POST["txtCabana"];
                    $fecha = $_POST["txtFecha"];

                    //Inserta valor de los inputs en la base de datos finca, tabla cliente.
                    mysqli_query($conexion, "insert into cliente(nombre, apellido, cabana, fecha) 
                          values ('$nombre','$apellido','$cabana', '$fecha')") or
                    die("Problemas en el select" . mysqli_error($conexion));
                }

                $registros = mysqli_query($conexion, "select idcliente, nombre, apellido, cabana, fecha from cliente") or
                die("Problemas en el select:" . mysqli_error($conexion));

                // Tabla 
                echo "<div class='container'>";
                echo "<table class='table table-bordered table-striped'>";
                echo "<tr>";
                echo "<td>Id</td>";
                echo "<td>Nombre</td>";
                echo "<td>Apellido</td>";
                echo "<td>Fecha Registro</td>";
                echo "<td>Cabaña</td>";
                echo "</tr>";

                while ($reg = mysqli_fetch_array($registros)) {
                    echo "<tr>";
                    echo "<td>" . $reg['idcliente'] . "</td>";
                    echo "<td>" . $reg['nombre'] . "</td>";
                    echo "<td>" . $reg['apellido'] . "</td>";
                    echo "<td>" . $reg['fecha'] . "</td>";
                    echo "<td>" . "<img src='img/" . $reg['cabana'] . ".jpg' width='25%'>" . "</td>";
                    echo "</tr>";
                }

                echo "</table>";
                echo "</div>";

                // cerrrar conexion
                mysqli_close($conexion);
            ?>
        </div>

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright Todos los Derechos Reservados | Finca Ranchos de los Montes</p>
            </div>
        </footer>

        <!-- Bootstrap y JavaScript -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    </body>
</html>